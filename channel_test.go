package main

import (
	"testing"
)

func TestIsPrivateChannel(t *testing.T) {
	if ChannelType("private-user.1") != PrivateChannel {
		t.Error("private-user.1 should be a private channel")
	}

	if ChannelType("presence-user.1") != PresenceChannel {
		t.Error("presence-user.1 should be a presence channel")
	}

	if ChannelType("user.1") != PublicChannel {
		t.Error("user.1 should be a public channel")
	}
}

func TestShouldCheckChannelAuth(t *testing.T) {
	if !ShouldCheckChannelAuth("private-user.1") {
		t.Fatal("private-user.1 should be checked for auth")
	}

	if !ShouldCheckChannelAuth("presence-user.1") {
		t.Fatal("presence-user.1 should be checked for auth")
	}

	if ShouldCheckChannelAuth("user.1") {
		t.Fatal("user.1 should not be checked for auth")
	}
}

func TestConnectionToChannel(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")

	if err := channels.subscribe(socket, "home"); err != nil {
		t.Fatal(err)
	}

	sockets := channels.socketsSubscribedTo("home")
	if len(sockets) != 1 || sockets[0].ID != "1234" {
		t.Fatal("expected socket 1234 to be connected to home")
	}
}

func TestMultipleConnectionAttempt(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")

	// First subscription should be ok
	if err := channels.subscribe(socket, "home"); err != nil {
		t.Fatal(err)
	}

	// Second and beyond should return an error
	if err := channels.subscribe(socket, "home"); err == nil {
		t.Fatal("expected second subscription to return an error")
	}

	// Only the first subscription should exist in the record
	if len(channels.socketsSubscribedTo("home")) != 1 {
		t.Fatal("expected only one connection on the home channel")
	}
}

func TestPresenceChannelSubscriptionIsStoredOnRedis(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")

	channels.connectToRedis("localhost:6379")

	// Presence channels are prefixed with "presence-"
	if err := channels.subscribe(socket, "presence-home"); err != nil {
		t.Fatal(err)
	}

	// the subscription should be stored in memory as well
	socketsInMemory := channels.socketsSubscribedTo("presence-home")
	if len(socketsInMemory) != 1 {
		t.Fatal("expected subscription to presence channels to be kept in memory")
	}

	// read connections from redis
	sockets, err := channels.getSubscriptionsFromRedis("presence-home")
	if err != nil {
		t.Fatal(err)
	}
	if len(sockets) != 1 || sockets[0].ID != "1234" {
		t.Fatal("expected socket connection to be stored on redis")
	}

	// clear redis
	conn := channels.redisPool.Get()
	defer conn.Close()
	if _, err := conn.Do("FLUSHALL"); err != nil {
		t.Fatal(err)
	}
}

func TestReadSubscriptionsFromRedisWhenEmpty(t *testing.T) {
	channels := newChannelRegistry("test")
	channels.connectToRedis("localhost:6379")

	sockets, err := channels.getSubscriptionsFromRedis("any-channel")
	if err != nil {
		t.Fatal(err)
	}
	if len(sockets) > 0 {
		t.Fatal("expected no subscriptions on channel")
	}
}

func TestSubscriptionIsRemovedFromMemory(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")

	if err := channels.subscribe(socket, "home"); err != nil {
		t.Fatal(err)
	}

	if err := channels.unsubscribe(socket, "home"); err != nil {
		t.Fatal(err)
	}

	sockets := channels.socketsSubscribedTo("home")
	if len(sockets) > 0 {
		t.Fatal("expected no connections after unsubscribe")
	}
}

func TestUnsubscribeDoesNothingIfNotSubscribed(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")

	err := channels.unsubscribe(socket, "any-channel")
	if err != socketNotSubscribedToChannelError {
		t.Fatal("expected error when unsubscribing from random channel")
	}
}

func TestSubscriptionIsRemovedFromRedis(t *testing.T) {
	channels := newChannelRegistry("test")
	channels.connectToRedis("localhost:6379")
	socket := NewSocket("1234")

	// subscribe to the channel
	if err := channels.subscribe(socket, "presence-home"); err != nil {
		t.Fatal(err)
	}

	// check the subscription is stored on redis
	sockets, err := channels.getSubscriptionsFromRedis("presence-home")
	if err != nil {
		t.Fatal(err)
	}
	if len(sockets) != 1 {
		t.Fatal("expected subscription to be stored on redis")
	}

	// unsubscribe from channel
	if err := channels.unsubscribe(socket, "presence-home"); err != nil {
		t.Fatal(err)
	}

	// check the subscription is removed from redis
	sockets, err = channels.getSubscriptionsFromRedis("presence-home")
	if err != nil {
		t.Fatal(err)
	}
	if len(sockets) > 0 {
		t.Fatal("expected subscription to be removed from redis")
	}
}
