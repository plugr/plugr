package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/garyburd/redigo/redis"
)

// event contains data that should be sent to sockets subscribed to a channel.
type event struct {
	Name    string                 `json:"name"`
	Channel string                 `json:"channel"`
	Data    map[string]interface{} `json:"data"`
}

// toJSON serializes the event to JSON
func (e event) toJSON() []byte {
	data, err := json.Marshal(e)
	if err != nil {
		log.Fatal("failed to marshal event to JSON")
		return []byte("")
	}
	return data
}

// listenForEventsOnRedis runs on it's own goroutine. It opens a connection to
// redis only for the pubsub.
func listenForEventsOnRedis(cr *channelRegistry) error {
	conn, err := cr.redisPool.Dial()
	if err != nil {
		return err
	}
	psc := redis.PubSubConn{Conn: conn}
	psc.PSubscribe("*")
	for {
		switch v := psc.Receive().(type) {
		case redis.PMessage:
			fmt.Printf("%s: message: %s\n", v.Channel, v.Data)
			event := event{}
			err = json.Unmarshal(v.Data, &event)
			if err == nil {
				event.Channel = v.Channel
				go cr.emit(event)
			} else {
				log.Fatalf("failed to parse event as JSON: %v", v.Data)
			}
		case redis.Subscription:
			fmt.Printf("%s: %s %d\n", v.Channel, v.Kind, v.Count)
		case error:
			return v
		}
	}
}
