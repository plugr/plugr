package main

func main() {
	// http.HandleFunc("/", WebsocketHandler)
	// log.Fatal(http.ListenAndServe(":8080", nil))

	channels := newChannelRegistry("test")
	channels.connectToRedis("localhost:6379")
	if err := listenForEventsOnRedis(channels); err != nil {
		panic(err)
	}
}
