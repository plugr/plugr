package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"strings"
	"sync"
	"time"

	"github.com/garyburd/redigo/redis"
)

const (
	PrivateChannel = iota
	PresenceChannel
	PublicChannel
)

var (
	socketNotSubscribedToChannelError = errors.New("socket not subscribed to channel")
)

// channelRegistry manages the connections to all channels in this server.
// There should be one registy per server. It is safe to call functions in
// this struct from multiple goroutines.
type channelRegistry struct {
	serverName string

	mutex       sync.Mutex
	connections map[string][]*Socket
	redisPool   *redis.Pool
}

// newChannelRegistry returns a ChannelRegistry with empty connections.
func newChannelRegistry(name string) *channelRegistry {
	return &channelRegistry{
		serverName:  name,
		connections: make(map[string][]*Socket),
	}
}

// connectToRedis initializes the client in the channel registry. This function
// should be called once in the initialization process.
func (cr *channelRegistry) connectToRedis(addr string) error {
	cr.redisPool = &redis.Pool{
		MaxIdle:     5,
		IdleTimeout: 240 * time.Second,
		// TODO: TestOnBorrow
		Dial: func() (redis.Conn, error) {
			return redis.Dial("tcp", addr)
		},
	}

	return nil
}

// subscribe stores the link between the socket and the channel in the
// connections map. If the channel is of the type PresenceChannel the
// connection is also stored in Redis in order to be made available on
// the PHP side.
//
// This function can be called from multiple goroutines.
func (cr *channelRegistry) subscribe(s *Socket, channel string) error {
	// TODO: we can optimize this by locking on the specific channel instead of
	// a global lock.
	cr.mutex.Lock()
	defer cr.mutex.Unlock()

	if val, ok := cr.connections[channel]; ok {
		for _, socket := range val {
			if socket.ID == s.ID {
				return fmt.Errorf("socket already joined channel: %v", channel)
			}
		}
		cr.connections[channel] = append(val, s)
	} else {
		cr.connections[channel] = []*Socket{s}
	}

	if ChannelType(channel) == PresenceChannel {
		if err := cr.updateChannelConnectionsOnRedis(channel); err != nil {
			return err
		}
	}

	return nil
}

func (cr *channelRegistry) unsubscribe(s *Socket, channel string) error {
	// TODO: we can optimize this by locking on the specific channel instead of
	// a global lock.
	cr.mutex.Lock()
	defer cr.mutex.Unlock()

	socketRemoved := false

	if val, ok := cr.connections[channel]; ok {
		for i, socket := range val {
			if socket.ID == s.ID {
				val[len(val)-1], val[i] = val[i], val[len(val)-1]
				cr.connections[channel] = val[:len(val)-1]
				socketRemoved = true
				break
			}
		}
	}

	if !socketRemoved {
		return socketNotSubscribedToChannelError
	}

	if socketRemoved && ChannelType(channel) == PresenceChannel {
		if err := cr.updateChannelConnectionsOnRedis(channel); err != nil {
			return err
		}
	}

	return nil
}

// updateChannelConnectionsOnRedis stores the connection state on a hash table
// on redis. This should only be used for presence channels because our PHP
// app may need to read this state. This is an operation that needs to be
// synced in order to avoid concurreny issues. This function is called from
// `storeConnection` which already uses a mutex.
// We don't need to sync across multiple nodes running on different machines
// because they'll use a different key name to store things on redis. The PHP
// library is prepared to read the connection state from multiple nodes.
func (cr channelRegistry) updateChannelConnectionsOnRedis(c string) error {
	conn := cr.redisPool.Get()
	defer conn.Close()

	sockets := cr.socketsSubscribedTo(c)

	if len(sockets) > 0 {
		data, err := json.Marshal(sockets)
		if err != nil {
			return err
		}

		_, err = conn.Do("HSET", cr.redisConnectionsKey(), c, data)
		return err
	} else {
		_, err := conn.Do("HDEL", cr.redisConnectionsKey(), c)
		return err
	}
}

// redisConnectionsKey returns the key used to store websocket connections for
// presence channels on redis.
func (cr channelRegistry) redisConnectionsKey() string {
	return "plugr-connections:" + cr.serverName
}

// emit sends the event to the sockets subscribed to the event's channel.
func (cr channelRegistry) emit(ev event) []*Socket {
	sockets := cr.socketsSubscribedTo(ev.Channel)
	socketsSent := make([]*Socket, 0)
	for _, socket := range sockets {
		if err := socket.emit(ev); err == nil {
			socketsSent = append(socketsSent, socket)
		}
	}
	return socketsSent
}

// getSubscriptionsFromRedis reads the sockets connected to the given channel
// from Redis. Only presence channels are persisted to redis.
func (cr channelRegistry) getSubscriptionsFromRedis(c string) ([]Socket, error) {
	conn := cr.redisPool.Get()
	defer conn.Close()

	data, err := conn.Do("HGET", cr.redisConnectionsKey(), c)
	if err != nil {
		return nil, err
	}

	// If the channel doesn't exist in the HMAP in redis it returns nil, in
	// which case we return an empty list of sockets.
	if data == nil {
		return []Socket{}, nil
	}

	var sockets []Socket
	bytes, ok := data.([]byte)
	if !ok {
		return nil, errors.New("invalid data returned from redis")
	}
	if err = json.Unmarshal(bytes, &sockets); err != nil {
		return nil, err
	}
	return sockets, nil
}

func (cr *channelRegistry) socketsSubscribedTo(channel string) []*Socket {
	if sockets, ok := cr.connections[channel]; ok {
		return sockets
	}

	return []*Socket{}
}

// ChannelType detects the type of the channel by the name. Private channels
// should be prefixed with "private-" and presence channels should be
// prefixed with "presence-".
func ChannelType(channel string) int32 {
	if strings.Index(channel, "private-") == 0 {
		return PrivateChannel
	}
	if strings.Index(channel, "presence-") == 0 {
		return PresenceChannel
	}
	return PublicChannel
}

// ShouldCheckChannelAuth returns true if the channel is either private or
// presence, false if public.
func ShouldCheckChannelAuth(channel string) bool {
	t := ChannelType(channel)
	if t == PrivateChannel || t == PresenceChannel {
		return true
	}
	return false
}
