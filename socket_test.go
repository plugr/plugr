package main

// How do we go about this? The 'conn' we get from gorilla's server has the
// function to send a message down the pipe. Is this the only functionality
// we need?

import "testing"

var subscribeMsg = []byte(`{"name": "subscribe", "channel": "home", "data": {}}`)
var unsubscribeMsg = []byte(`{"name": "unsubscribe", "channel": "home", "data": {}}`)
var randomMsg = []byte(`{"name": "anything-else", "channel": "", "data": {}}`)

func TestSubscribeToChannel(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")
	socket.registry = channels

	// simulate message sent from the client
	if err := socket.handleMessage(subscribeMsg); err != nil {
		t.Fatal(err)
	}

	// check the socket is stored in the registry
	sockets := channels.socketsSubscribedTo("home")
	if len(sockets) != 1 {
		t.Error("expected socket to be subscribed to channel")
	}

	// check the channel is stored in the socket
	if socket.ConnectedChannels[0] != "home" {
		t.Error("expected channel to be stored in the socket as well")
	}
}

func TestUnsubscribeToChannel(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")
	socket.registry = channels

	if err := socket.handleMessage(subscribeMsg); err != nil {
		t.Fatal(err)
	}
	if err := socket.handleMessage(unsubscribeMsg); err != nil {
		t.Fatal(err)
	}

	// check the socket is removed from the registry
	sockets := channels.socketsSubscribedTo("home")
	if len(sockets) != 0 {
		t.Error("expected socket to be removed from the registry")
	}

	// check the channel is removed from the socket
	cs := socket.ConnectedChannels
	if len(cs) != 0 {
		t.Fatal("expected channel to be removed from the socket")
	}
}

func TestHandleMessageErrorOnUnsupportedFormat(t *testing.T) {
	socket := NewSocket("1234")

	if err := socket.handleMessage(randomMsg); err == nil {
		t.Fatal("expected unsupported msg to return error")
	}
}
