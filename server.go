package main

import (
	"log"
	"net/http"

	"github.com/gorilla/websocket"
)

var websocketUpgrader = websocket.Upgrader{
	ReadBufferSize:  1024,
	WriteBufferSize: 1024,
	CheckOrigin: func(r *http.Request) bool {
		return true
	},
}

func generateSocketID() string {
	// TODO: implement
	return "1234"
}

func WebsocketHandler(w http.ResponseWriter, r *http.Request) {
	conn, err := websocketUpgrader.Upgrade(w, r, nil)

	if err != nil {
		log.Panic(err)
	}

	socket := NewSocket(generateSocketID())
	socket.conn = conn

	for {
		messageType, p, err := conn.ReadMessage()
		if err != nil {
			log.Panic(err)
		}
		// TODO: handle message
		if err := conn.WriteMessage(messageType, p); err != nil {
			log.Panic(err)
		}
	}
}
