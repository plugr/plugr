package main

import (
	"testing"
)

func TestEventIsBroadcastedToSubscribedSockets(t *testing.T) {
	channels := newChannelRegistry("test")
	socket := NewSocket("1234")

	channels.subscribe(socket, "home")

	ev := event{Channel: "home", Name: "any"}
	socketsSent := channels.emit(ev)

	if len(socketsSent) != 1 {
		t.Fatalf("expected event to be sent to 1 socket, but was sent: %v",
			len(socketsSent))
	}

	if len(socket.events) != 1 {
		t.Fatal("expected one event in the socket queue")
	}
	if socket.events[0].Channel != "home" {
		t.Fatal("expected event to be in the 'home' channel")
	}
}
