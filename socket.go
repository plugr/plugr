package main

import (
	"encoding/json"
	"fmt"
	"strings"

	"github.com/gorilla/websocket"
)

// channelConnections is an index that maps the channel name to the connected
// sockets. The connection to all channels (including private and presence)
// are stored in this map.
var channelConnections = make(map[string][]*Socket)

// Socket represents a websocket connection between a client and the server.
// Each connection has it's own socket, so it may happen that the same user
// has multiple connections if they have multiple tabs open (or they're
// connected from desktop and mobile).
type Socket struct {
	// Unique ID of the socket. This value is generated on connection.
	ID string `json:"id"`

	// Headers we'll send to the auth server to authorize connection to private
	// channels.
	AuthHeaders map[string]string `json:"-"`

	// Cached list of the channels the socket is connected to.
	ConnectedChannels []string `json:"-"`

	// The underlying websocket connection provided by gorilla/weboskcet
	// we'll use to send messages to. Each websocket connection is managed
	// by a different goroutine.
	conn *websocket.Conn

	// events is an array of messages sent from the server to the socket. This
	// only used in tests and array will be *empty* on production. I don't
	// the overhead of an empty array is significant, so this will do for now.
	events []event

	// registry this socket is stored on. There should only be one registry
	// per server.
	registry *channelRegistry
}

// NewSocket returns a new Socket instance. The socket is not connected to
// any channel by default.
func NewSocket(id string) *Socket {
	return &Socket{
		ID:                id,
		AuthHeaders:       make(map[string]string),
		ConnectedChannels: make([]string, 0),
		events:            make([]event, 0),
	}
}

// emit sends the event to the socket.
func (s *Socket) emit(ev event) error {
	if s.conn == nil {
		s.events = append(s.events, ev)
	} else {
		if err := s.conn.WriteMessage(websocket.TextMessage, ev.toJSON()); err != nil {
			return err
		}
	}
	return nil
}

// handleMessage is the function called for events received directly from the
// client. The client sends messages to the websocket to perform one of the
// following actions:
//
// * subscribe to a channel
//   `{"name": "subscribe", "channel": "desired-channel", "data": {}}`
// * unsubscribe from a channel
//   `{"name": "unsubscribe", "channel": "desired-channel", "data": {}}`
// * whisper
//   `{"name": "whisper-typing", "channel": "desired-channel", "data": {}}`
func (s *Socket) handleMessage(msg []byte) error {
	ev := event{}
	err := json.Unmarshal(msg, &ev)
	if err != nil {
		return err
	}

	if ev.Name == "subscribe" {
		s.storeChannel(ev.Channel)
		return s.registry.subscribe(s, ev.Channel)
	} else if ev.Name == "unsubscribe" {
		s.removeChannel(ev.Channel)
		return s.registry.unsubscribe(s, ev.Channel)
	} else if strings.Index(ev.Name, "whisper-") == 0 {
		// TODO
	}

	return fmt.Errorf("unsuported message from socket: %v", msg)
}

// IsAuthorizedToConnect sends an HTTP request to the auth server to check if
// the socket can or cannot connect to the channel.
func (s *Socket) IsAuthorizedToConnect(channel string) bool {
	// TODO: send http request
	return true
}

func (s *Socket) storeChannel(channel string) {
	s.ConnectedChannels = append(s.ConnectedChannels, channel)
}

func (s *Socket) removeChannel(channel string) {
	for i, c := range s.ConnectedChannels {
		if c == channel {
			arr := s.ConnectedChannels
			arr[len(arr)-1], arr[i] = arr[i], arr[len(arr)-1]
			s.ConnectedChannels = arr[:len(arr)-1]
			break
		}
	}
}
